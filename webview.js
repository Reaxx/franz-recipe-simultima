module.exports = (Franz) =>
{
	let lastDirectCount = 0;
	let lastIndirectCount = 0;

	//When the windows is clicked, reset badge and set lastcount
	window.onclick = function ()
	{
		Franz.setBadge(0);
		lastDirectCount = countDirectMessages();
		lastIndirectCount = countIndirectMessages();
	};

	//Counts all direct messeges not sent by user
	function countDirectMessages()
	{
		//Find all direct messages ([style*="#FF0000"])
		let allMsg = document.querySelectorAll('span.avatar [style*="#FF0000"] ').length;
		//Find all active users direct messeges (both span.green and [style*="#FF0000"])
		let myMsg = document.querySelectorAll('span.avatar span.green ~ [style*="#FF0000"]').length;

		return allMsg-myMsg;
	}
	//Counts all messeages not sent by user
	function countIndirectMessages()
	{
		//Find all messeages
		let allMsg = document.querySelectorAll('div.message').length;
		//Find all active users messes (class="green")
		let myMsg = document.querySelectorAll('span.avatar span.green').length;
		//Return Diff
		return allMsg-myMsg;
	}


	const getMessages = function getMessages()
	{
		let newDirectcount = countDirectMessages();
		let directDiff = newDirectcount - lastDirectCount;

		let newIndirectcount = countIndirectMessages();
		let indirectDiff = newIndirectcount - lastIndirectCount

			console.log(directDiff+" "+indirectDiff)

		if(indirectDiff > 0)
		{
			Franz.setBadge(directDiff,indirectDiff);
		}
	};

	Franz.loop(getMessages);
};
